(define-library
  (retropikzel display-util v0-1-0 main)
  (import (scheme base)
          (scheme write))
  (export display-util-displine)
  (begin
    (define display-util-displine
      (lambda (to-display)
        (display to-display)
        (newline)))))
